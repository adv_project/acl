#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

$BOLD = "\0112";
$NOTBOLD = "\0115";

sub callSendMeInfo {
  my $message = shift;
  my $object = {
    "class"   => "send",
    "player"  => $input->{'player'},
    "type"    => "info",
    "message" => $message
  };
  return $object;
}

sub callKick {
  my $player = shift;
  my $object = {
    "class"   => "kick",
    "player"  => $player
  };
  return $object;
}

sub callAbort {
  my $object = { "class" => "abort" };
  return $object;
}

1;
