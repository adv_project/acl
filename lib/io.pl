#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use JSON;
use DBI;
use Storable qw(dclone);

sub readGameProperties {
  local $/;
  open my $fh, '<', $gamepropertiesfile;
  my $json = <$fh>;
  close $fh;
  $gameproperties = JSON::decode_json($json);
}

sub readInput {
  local $/;
  open my $fh, '<', $inputfile;
  my $json = <$fh>;
  close $fh;
  $input = JSON::decode_json($json);
}

sub writeOutput {
  my $output = dclone(\@calls);
  my $json = JSON->new->pretty(1)->encode($output);
  open my $fh, '>', $outputfile;
  print $fh $json;
  close $fh;
}

sub readState {
  local $/;
  open my $fh, '<', $aclfile;
  my $json = <$fh>;
  close $fh;
  if ($json) {
    $acl = JSON::decode_json($json);
  } else {
    my @array = ();
    $acl = {
      "acl" => dclone(\@array),
      "kick" => ""
    }
  }
}

sub writeState {
  my $json = JSON->new->pretty(1)->encode($acl);
  open my $fh, '>', $aclfile;
  print $fh $json;
  close $fh;
}

1;
