# Access Control List for Adv Games

Use this mod to control who can enter your games.

Put this mod first when creating a game and use the
`manage` command to invite or ban players.

The game owner can always enter the game.

